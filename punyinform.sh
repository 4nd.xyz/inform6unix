#!/bin/sh
#
# This script passes the correct include_path to the Inform6 compiler
# for building games using the PunyInform Library.  Normally this script
# is automatically configured by the installing Makefile.
#
# This script was written by David Griffith <dave@661.org> in 2021 and
# released to the public domain.

# This is usually where the Inform6 libraries are installed.
# The installation process will fix this if necessary.
LIBPATH=/usr/local/share/inform

INFORM=inform
LIB=punyinform
NEWPATH=$LIBPATH/$LIB/lib

ZM_VERSION=-v3

ARGV=$@
ARGC=0
for ARG in $@; do
	ARGC=`expr $ARGC + 1`
	case $ARG
	in
		-v*)
			ZM_VERSION=$ARG
			ARGV=`echo $ARGV | cut -d" " --complement -f $ARGC`
			break
		;;
	esac

done
$INFORM $ZM_VERSION +$NEWPATH $ARGV

